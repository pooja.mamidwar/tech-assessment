def get_value_from_nested_object(obj, key):
    if isinstance(obj, dict):
        if key in obj:
            return obj[key]
        for value in obj.values():
            result = get_value_from_nested_object(value, key)
            if result is not None:
                return result
    elif isinstance(obj, list):
        for item in obj:
            result = get_value_from_nested_object(item, key)
            if result is not None:
                return result
    return None

#Assuming you have a nested object like this:
nested_object = {
    "a": "Hello",
    "b": "World",
    "c": {
         "x": 42,
         "y": 55
     }
 }

# To get the value associated with the key "x" from the nested_object:
value = get_value_from_nested_object(nested_object, "x")
print(value)  # Output: 42
